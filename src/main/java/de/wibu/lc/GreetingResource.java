package de.wibu.lc;


import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class GreetingResource {

    @Inject
    GreetingService service;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/greeting/{name}")
    public String greeting(@PathParam("name") String name) {
        return service.greeting(name);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/hello")
    public String hello() {
        return "hello1";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/loaderio-b223076b09deb8d89e2c85f6a32053b7/")
	public String loader() {
		return "loaderio-b223076b09deb8d89e2c85f6a32053b7/";
	}
}