package de.wibu.lc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.SocketAddress;
import java.net.URL;
import java.util.stream.IntStream;

public class Tester {

	public static void main(String... arg) throws Exception {
		
		URL url = new URL("http://35.230.147.151/hello");

		long time = System.currentTimeMillis();
		
		for (int i = 0; i < 1000; i++) {
	
			SocketAddress sa = new InetSocketAddress("proxy.wibu.local", 8080);
			Proxy proxy = new Proxy(Type.HTTP, sa);
			
			HttpURLConnection con = (HttpURLConnection)url.openConnection(proxy);
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			//print result
			System.out.print(".");
		}
		
		time = System.currentTimeMillis() - time;
		
		//print result
		System.out.println(time + " ms");
		
	}
}
